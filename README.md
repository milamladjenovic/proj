Projekat na kome su primenjivani alati: https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/08-mainant

# Alati
1. [Cmake](https://gitlab.com/milamladjenovic/proj/-/issues/1)

2. Git (pogledati na linku: https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/08-mainant)

3. [Valgrind](https://gitlab.com/milamladjenovic/proj/-/issues/3)

4. [Clang Tidy](https://gitlab.com/milamladjenovic/proj/-/issues/5)

5. [Clang Format](https://gitlab.com/milamladjenovic/proj/-/issues/6)

6. [Staticka analiza koda - Clazy](https://gitlab.com/milamladjenovic/proj/-/issues/7)

7.  [GDB](https://gitlab.com/milamladjenovic/proj/-/issues/8)

8. [Git Hooks](https://gitlab.com/milamladjenovic/proj/-/issues/9)

9. [GCov](https://gitlab.com/milamladjenovic/proj/-/issues/10)
